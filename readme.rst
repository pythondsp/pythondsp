See below link, for complete list of tutorials,
***********************************************

http://pythondsp.readthedocs.io/


Table of Contents 
*****************

In this website, following programming languages are used for different purposes, 

* **C/C++** : 'To simulate the mathematical models of the design' and 'implementing the designs on FPGA using NIOS-II processor.'
* **Verilog/SystemVerilog/VHDL** : To implement the designs on FPGA board. 
* **Python** : 'To simulate the mathematical models of the designs', 'data processing' and for analyzing the data received by the FPGA'. 
* **MATLAB** : 'To simulate the mathematical models of the designs'. 
* **HTML and JavaScript etc.** : 'To design the webpages.'


.. admonition:: Offline tutorials in PDF, EPUB and HTML formats
   :class: danger

   Click on the '**v.latest**' on the bottom-left in the tutorial website, and select the format for offline reading, as shown in below figure, 

   .. figure:: fig/download.png
   
      Download tutorials for offline reading 


Teachings and Quotes
====================

* `Goodread <https://goodread.readthedocs.io/en/latest/>`_
* `Meher Baba Prayers <https://meher-baba-prayers.readthedocs.io/en/latest/>`_ (`Download Kindle-format <https://drive.google.com/open?id=0B9zymgX8PsIXczFPRzJCOHdRTWc>`_)
  

Linux software list
===================

A list of software is added in the below link, which are used to create the tutorials presented in this website. The software lists are provided for 'Ubuntu', 'Lubuntu', 'Mint' and 'Fedora'.

.. note::

    I tried various other distros as well e.g. CentOS, Manjaro and OpenSUSE etc. But I like "Lubuntu" as it is lightweight and the software (related to Electronics designs) are very easy to install as compare to other distros. 

* `Linux software list <https://linux-software-list.readthedocs.io/en/latest/>`_
    
FPGA Designs with VHDL
======================

In this tutorial, VHDL is used to implement various designs on FPGA board. All the designs are tested on the FPGA boards. NIOS II processor is also used for designs. 

* Online : https://vhdlguide.readthedocs.io


.. _`fpga-designs-with-verilog-and-systemverilog`:

FPGA designs with Verilog and SystemVerilog
===========================================

In this tutorial, Verilog is used to implement various designs on FPGA board. All the designs are tested on the FPGA boards. NIOS II processor is also used for designs. 

* Online : https://verilogguide.readthedocs.io

FPGA designs with MyHDL
=======================

In this tutorial, the designs of `Verilog/SystemVerilog <https://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html#fpga-designs-with-verilog-and-systemverilog>`_/`VHDL <https://pythondsp.readthedocs.io/en/latest/pythondsp/toc.html#fpga-designs-with-vhdl>`_-tutorials are re-implemented using MyHDL. 

* Online  : https://myhdlguide.readthedocs.io


Programming with C and C++
==========================

In this tutorial, basic features of C and C++ programming are discussed for designing the embedded systems using NIOS.

* Online : https://cppguide.readthedocs.io

OpenCV
======

Some basic examples of OpenCV using Python, C and Petalinux

* `OpenCV guide <https://opencvguide.readthedocs.io>`_ 

Python
======

In the below topics, several features of Python are discussed which can be useful for 'data processing' and 'simulation of mathematical designs". 

Advance Python
--------------

In this tutorial, basic Python features are reviewed first. And then the advance Python features are discussed such as decorator, descriptor and property etc.  

* `Advance Python Tutorials <https://pythonguide.readthedocs.io/en/latest/>`_


Python for simulation
---------------------

In this tutorial, Python is discussed for simulating the mathematical designs. Further, Numba and Cython are used to speed up the simulation; and Matplotlib is used for plotting the data. Lastly, OOP method is also discussed and used for simulations. 

* :download:`Donwnload PDF <downloads/Python3-Tutorial.pdf>`


Matplotlib
----------

In this tutorial, the Matplotlib library is discussed to plot the data in Python, 

* `Matplotlib Guide <https://matplotlibguide.readthedocs.io/en/latest/>`_


Pandas-0.22.0
-------------

In this tutorial, Pandas library is discussed for data processing. Created using Python-3.6.4 and Pandas-0.22.0

* `Pandas guide <https://pandasguide.readthedocs.io/en/latest/index.html>`_

Machine learning
----------------

In this tutorial, the SciKit library is discussed for Machine learning.

* `Machine learning guide <https://mclguide.readthedocs.io/en/latest/>`_

Tensorflow
----------

In this tutorial, the Tensorflow is discussed for Machine learning.

* `Tensorflow guide <https://tensorflowguide.readthedocs.io/en/latest/>`_


Regular expressions
-------------------

Some useful regular expression patterrns are shown here, 

* `Regular expression guide <https://regexguide.readthedocs.io/en/latest/index.html>`_
  
Statistical analysis
--------------------

Here, basics of random variable and their probability distribution functions are discussed. Then PDF and CDF of these distributions are implemented using Python. 

* `Stochastic processes and data mining with python <https://DataMiningGuide.readthedocs.io/en/latest/index.html>`_
  

Testing with PyTest
-------------------

In the below tutorial, 'PyTest' library is used to test the Python and Cython codes. 

Also, we can see the methods by which Cython codes can be documented on the ReadTheDocs (see conf.py on the Git-repository). Please see the `'Documentation Guide' <https://docguide.readthedocs.io/en/latest/index.html>`_ for more details about the auto-documentation of Python and Cython codes. 

* `PyTest Guide : py.test and numpy.testing <https://pytestguide.readthedocs.io/en/latest/index.html>`_
  

GUI (under progress)
--------------------

* `GUI with PyQT and Tkinter <https://guiguide.readthedocs.io/en/latest/index.html>`_

MySQL with Python
=================

Here, various MySQL commands are discussed. Further, Python is used to connect with the MySQL database. 

* `MySQL with python guide <https://mysqlguide.readthedocs.io/en/latest/index.html>`_


MATLAB
======

Some examples of MATLAB, 

`MATLAB guide <https://matlabguide.readthedocs.io>`_

Guides
======
 
 Following are the short guides, which contain some useful tips/information for using Git, Unix and ReadTheDocs. Also, good ways of creating the simulators are discussed. 

Simulation guide
----------------

* `Simulation Guide : Guideline for designing simulators <https://simulationguide.readthedocs.io/en/latest/index.html>`_

Documentation guide
-------------------

* `Documentation Guide : Upload cython and python autodoc on ReadTheDoc <https://docguide.readthedocs.io/en/latest/index.html>`_

Git guide
---------

* `Git Guide <https://gitguide.readthedocs.io/en/latest/index.html>`_

Unix guide
----------

* `Unix Guide <https://unixguide.readthedocs.io/en/latest/index.html>`_


Text-editors (VIM and Sublime)
==============================

In these tutorials, keyboard-shortcuts for the text-editors are shown, 

* `Vim <https://vimguide.readthedocs.io/en/latest/index.html>`_

* `Sublime <https://sublimeguide.readthedocs.io/en/latest/index.html>`_
  

WEB Design
==========

This part contains various tutorials on web-designs, 

HTML, CSS, Bootstrap, JavaScript and jQuery
-------------------------------------------

Below link contains the tutorials on front-end web designs, 

* `Front-end tutorials <https://htmlguide.readthedocs.io>`_

Flask
-----

* `Flask <https://flaskguide.readthedocs.io>`_

Django
------

* `Bookstore using Django 1.11.10 (More on class-based-views) <https://djangoguide.readthedocs.io/en/latest/>`_
* `Bookstore using Django 1.8 (basics) <https://codehubdjango18.readthedocs.io/en/latest/>`_
* `Django with MySQL (old tutorial) <https://codehubpython.appspot.com/django>`_

Selenium testing
----------------

* `MySql database and Selenium testing <https://codehubpython.appspot.com/>`_


TMUX for terminals
==================

TMUX can be used for providing the addition features to terminals such as split-screen etc.

* `TMUX Guide <https://tmuxguide.readthedocs.io/en/latest/>`_


Latex format for Thesis and Notes
=================================

* Latex format can be downloaded from below link (go to link and click on '**clone and download**' and '**Download Zip file**'),

        https://bitbucket.org/pythondsp/latex-format-for-thesis-and-notes.git 

    

Sphinx format for Latex and html
================================

Here, conf.py is modified for Latex and HTML documents. Also, some of the methods are discussed for creating document using Sphinx. 

* Online : https://sphinxguide.readthedocs.io/en/latest/
* Git: https://bitbucket.org/pythondsp/sphinx-format-for-latexpdf-html.git